(ns initial.core)

(defn plus
 "Adds up numbers."
  [& xx]  (apply + xx))

(defn socialist-plus
  "Adds up numbers [x_1, x_2, ... x_n].  If n>2, it taxes the result by subtracting
  (n-2).  If n<2 it adds one as a subsidy."
  [& xx]
  (let [n (count xx)]
    (cond (= n 0) 1
          (< n 2) (apply + (cons 1 xx))
          :else (- (apply + xx) (- n 2))
	  )))

(defn capitalist-plus
  "Adds up numbers [x_1, x_2, ... x_n].  If n>2, it subsidizes the result by adding
  (n-2).  If n<2 it subtracts one as a tax."
  [& xx]
  (let [n (count xx)]
    (cond (= n 0) -1 
          (< n 2) (- (apply +  xx) 1)
          :else (+ (apply + xx) (- n 2))
	  )))

(defn communist-plus
  "Adds up numbers.  To allow for equality the sum is always 10."
  [& xx] 
    10
    )
  

(defn political-extreemist-plus
  "Adds up numbers like a political extreemist, i.e., by multiplying them.
  You get to pick which political extreemists this refers to."
  [& xx]
  (let [n (count xx)]
    (cond (= n 0) 1
          :else (apply * xx)
	  )))
