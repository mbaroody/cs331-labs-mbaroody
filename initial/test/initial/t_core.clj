(ns initial.t-core
  (:use midje.sweet)
  (:use [initial.core]))

(facts "about plus"
       (fact "it adds numbers"
             (plus) => 0
             (plus 10) => 10
             (plus 10 20) => 30
             (plus 10 20 30 40 50) => 150
             (plus -10 -10 -10) => -30
             )
       )

(facts "about socialist plus"
       (fact "it subsidized fewer than two elements"
             (socialist-plus) => 1
             (socialist-plus 10) => 11
             (socialist-plus 9) => 10
             (socialist-plus -1) => 0
             (socialist-plus 0) => 1
             )
       (fact "it does nothing with two elements. Except add."
             (socialist-plus 10 20) => 30
             (socialist-plus 60 9) => 69
             (socialist-plus -10 -10) => -20
             )
       (fact "it taxes the result if there are more than 2 elements."
             (socialist-plus 10 20 30) => 59
             (socialist-plus 10 20 30 40 50) => 147
             (socialist-plus 10 20 30 40 50 60 70 80) => 354
             (socialist-plus 100 100 100 100 100 100 100 100 100 100 100 100) => 1190
             (socialist-plus -10 -10 -10) => -31
             ))

(facts "about capitalist plus"
       (fact "it taxes fewer than two elements"
             (capitalist-plus) => -1
             (capitalist-plus 12) => 11
             (capitalist-plus 0) => -1
             (capitalist-plus -1) => -2
             (capitalist-plus 13) => 12
             (capitalist-plus 14) => 13
             (capitalist-plus -10) => -11
             (capitalist-plus 1) => 0
             )
       (fact "it does nothing with two elements. Except add."
             (capitalist-plus 1 1) => 2
             (capitalist-plus 2 2) => 4
             (capitalist-plus 68 1) => 69
             (capitalist-plus 13 56) => 69
             (capitalist-plus 48 21) => 69
             (capitalist-plus -10 -10) => -20
             )
       (fact "it subsidizes the result if there are more than 2 elements."
             (capitalist-plus 1 1 1 1 1 1 1 1 1) => 16
             (capitalist-plus 10 20 30 40 50 60) => 214
             (capitalist-plus 100 100 100 100) => 402
             (capitalist-plus 10 10 10 10 32) => 75
             (capitalist-plus -10 -10 -10) => -29
             ))

(facts "about communist plus"
       (fact "it only returns 10."
             (communist-plus) => 10
             (communist-plus 23 23 23) => 10
             (communist-plus 32423 234 234234 2) => 10
             (communist-plus -2345) => 10
             ))

(facts "about political extreemist plus"
       (fact "it multiplies instead of adds."
             (political-extreemist-plus) => 1
             (political-extreemist-plus 1) => 1
             (political-extreemist-plus -13 13) => -169
             (political-extreemist-plus 10 10 10 10 10) => 100000
             (political-extreemist-plus -10 -10) => 100
             ))
