Grade Report for Michael W. Baroody
Produced 2014-12-15 11:41
Netid: mbaroody cwid: A20341101
Course: cs331 (2014-08)  Section: 02

     Assignment   Status Raw Max    Norm Weight  Points 
--------------- -------- --- --- ------- ------ ------- 
    Exam 1 (v1) Replaced n/a  92  83.00% 0.2500 20.7500 
    Exam 2 (v1)   Graded  78  84  92.86% 0.2500 23.2143 
Final Exam (v1)   Graded 166 200  83.00% 0.2500 20.7500 
    Initial Lab   Graded 100 100 100.00% 0.0625  6.2500 
Linked List Lab   Graded 100 100 100.00% 0.0625  6.2500 
  Traversal Lab   Graded  90 100  90.00% 0.0625  5.6250 
      Deque Lab   Graded 100 100 100.00% 0.0625  6.2500 
       Heap Lab  Dropped   5 100   5.00% 0.0000  0.0000 
--------------- -------- --- --- ------- ------ ------- 
          Total                   89.09% 1.0000 89.0893 
