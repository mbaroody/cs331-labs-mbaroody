(ns deque.t-core
  (:use midje.sweet)
  (:use [deque.core])
  (:import [deque.core Deque] ))

(facts "about this lab"
  (fact "the student never started it."
        (+ 1 2)  => 3))

(def aDeque (Deque. '(1) '(5 4 3 2) 5))

(facts "about push front"
  (fact "it increments size once"
        (:size (push-front (make-deque) 1)) => 1)
  (fact "it increments size twice"
        (:size (push-front (push-back (make-deque) 2) 1)) => 2)
  (fact "it will to the front list, not the back"
        (:front (push-front aDeque 0)) => '(0 1)
        (:back (push-front aDeque 0)) => '(5 4 3 2)
        (:size (push-front aDeque 0)) => 6))

(facts "about push back"
  (fact "it increments size once"
        (:size (push-back (make-deque) 1)) => 1)
  (fact "it increments size twice"
        (:size (push-back (push-back (make-deque) 2) 1)) => 2)
  (fact "it will push to the back list, not the front"
        (:front (push-back aDeque 6)) => '(1)
        (:back (push-back aDeque 6)) => '(6 5 4 3 2)
        (:size (push-back aDeque 6)) => 6))

(def a (Deque. '(1) '(5 4 3 2 1) 6))
(def b (Deque. '() '(5 4 3 2 1) 5))
(def emptee (make-deque))

(facts "about flip-front" 
  (fact "it doesn't flip if front not empty"
        (:front (flip-front a)) => '(1)
        (:back  (flip-front a)) => '(5 4 3 2 1)
        (:size (flip-front a)) => 6)
  (fact "it does flip if front empty"
        (:front (flip-front b)) => '(1 2 3 4 5)
        (:back (flip-front b)) => '()
        (:size (flip-front b)) => 5)
  (fact "it does nothing with an empty deque"
        (:front (flip-front emptee)) => '()
        (:back (flip-front emptee)) => '()
        (:size (flip-front emptee)) => 0))

(def a (Deque. '(6 5 4 3 2 1) '(1) 7))
(def b (Deque. '(6 5 4 3 2 1) '() 6))

(facts "about flip-back"
  (fact "it doesn't flip if back empty"
        (:front (flip-back a)) => '(6 5 4 3 2 1)
        (:back (flip-back a)) => '(1)
        (:size (flip-back a)) => 7)
  (fact "it does flip if back empty"
        (:front (flip-back b)) => '()
        (:back (flip-back b)) => '(1 2 3 4 5 6)
        (:size (flip-back b)) => 6)
  (fact "it does nothing with an empty deque"
        (:front (flip-back emptee)) => '()
        (:back (flip-back emptee)) => '()
        (:size (flip-back emptee)) => 0))

(def a (Deque. '(1 2 3 4) '(5) 5))
(def b (Deque. '() '(5 4 3 2 1) 5))

(facts "about front"
  (fact "it shows front without flipping if front occupied"
        (front a) => 1)
  (fact "it shows front and flips if front empty"
        (front b) => 1)
  (fact "it returns nil if passed an empty deque"
        (front emptee) => nil))

(def a (Deque. '(1 2 3 4) '(5) 5))
(def b (Deque. '(1 2 3 4 5) '() 5))

(facts "about back"
  (fact "it shows back without flipping if back occupied"
        (back a) => 5)
  (fact "it shows back and flips if back empty"
        (back b) => 5)
  (fact "it returns nill if passed an empty deque"
        (back emptee) => nil))

(def a (Deque. '(1) '(5 4 3 2) 5))
(def b (Deque. '() '(5 4 3 2 1) 5))
(def c (Deque. '() '(5) 1))

(facts "about pop-front"
  (fact "it pops front without flipping if front occupied"
        (:front (pop-front a)) => '() 
        (:back (pop-front a)) => '(5 4 3 2)
        (:size (pop-front a)) => 4)
  (fact "it pops front and flips if front empty"
        (:front (pop-front b)) => '(2 3 4 5)
        (:back (pop-front b)) => '()
        (:size (pop-front b)) => 4)
  (fact "it returns empty deque if one left"
        (:front (pop-front c)) => '()
        (:back (pop-front c)) => '()
        (:size (pop-front c)) => 0)
  (fact "pop-front behaves on an empty list"
        (:front (pop-front emptee)) => '()
        (:back (pop-front emptee)) => '()
        (:size (pop-front emptee)) => 0))

(def a (Deque. '(1 2 3 4) '(5) 5))
(def b (Deque. '(1 2 3 4 5) '() 5))
(def c (Deque. '(1) '() 1))

(facts "about pop-back" 
  (fact "it pops back without flipping if back occupied"
        (:front (pop-back a)) => '(1 2 3 4)
        (:back (pop-back a)) => '()
        (:size (pop-back a)) => 4)
  (fact "it pops back and flips if back empty"
        (:front (pop-back b)) => '()
        (:back (pop-back b)) => '(4 3 2 1)
        (:size (pop-back b)) => 4)
  (fact "it returns empty deque if one left"
        (:front (pop-back c)) => '()
        (:back (pop-back c)) => '()
        (:size (pop-back c)) => 0)
  (fact "it behaves on an empty list"
        (:front (pop-back emptee)) => '()
        (:back (pop-back emptee)) => '()
        (:size (pop-back emptee)) => 0))


