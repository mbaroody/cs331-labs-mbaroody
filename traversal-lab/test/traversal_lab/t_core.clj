(ns traversal_lab.t-core
  (:use midje.sweet)
  (:use [traversal_lab.core]))

(facts "about the student"
  (fact "they never got started."
        (+ 1 2)  => 3))

(def sm_ten (solve-maze-dfs (generate-maze 10 10) [0 0] [9 9]))
(def sm_nine (solve-maze-dfs (generate-maze 9 9) [0 0] [8 8]))
(def sm_eight (solve-maze-dfs (generate-maze 8 8) [2 2] [5 5]))
(def sm_seven (solve-maze-dfs (generate-maze 7 7) [2 3] [4 4]))
(def sm_seven_by_six (solve-maze-dfs (generate-maze 7 6) [2 2] [5 4]))
(def sm_six (solve-maze-dfs (generate-maze 6 6) [0 0] [5 5]))
(def sm_five (solve-maze-dfs (generate-maze 5 5) [0 0] [4 4]))

(defn multiple-solutions?
  "checks if node has solution bits in more than one direction"
  [node] 
  (if (< 1 (count (filter identity 
                          (list 
                            (get-solution node :w)
                            (get-solution node :s)
                            (get-solution node :e)
                            (get-solution node :n))))) true false)) 

(defn solution-at-wall?
  "iterates through maze and sees if there exists a solution and wall
   in the same direction... note that a border is a wall"
  [maze]
          (loop [r 0
                 c 0]
            (let [node (get-2d maze r c)]
              (cond (and (get-border node :s) (get-border node :e)) (or (and (get-solution node :w) (get-wall node :w))
                                                                        (and (get-solution node :s) (get-wall node :s))
                                                                        (and (get-solution node :e) (get-wall node :e))
                                                                        (and (get-solution node :n) (get-wall node :n)))
                    (get-border node :e) (if (or (and (get-solution node :w) (get-wall node :w))
                                                 (and (get-solution node :s) (get-wall node :s))
                                                 (and (get-solution node :e) (get-wall node :e))
                                                 (and (get-solution node :n) (get-wall node :n))) true (recur (inc r)
                                                                                                              0))
                    :else (if (or (and (get-solution node :w) (get-wall node :w))
                                  (and (get-solution node :s) (get-wall node :s))
                                  (and (get-solution node :e) (get-wall node :e))
                                  (and (get-solution node :n) (get-wall node :n))) true (recur r
                                                                                               (inc c)))))))
(defn more-than-one-solution?
  "iterates through maze to see if a node has solution bits 
   in more than one direction"
  [maze]
          (loop [r 0
                 c 0]
            (let [node (get-2d maze r c)]
             (cond (and (get-border node :s) (get-border node :e)) (multiple-solutions? node)
                   (get-border node :e) (if (multiple-solutions? node) true (recur (inc r)
                                                                                   0))
                   :else (if (multiple-solutions? node) true (recur r
                                                                    (inc c)))))))

(facts "about dfs solver"
  (fact "it solves a 3 X 3 maze one west"
         (solve-maze-dfs [[157 21 51] [139 9 38] [204 68 103]] [0 0] [0 1]) => 
           [[669 21 51] [139 9 38] [204 68 103]])
  (fact "it solves a 3 X 3 maze two west"
         (solve-maze-dfs [[157 21 51] [139 9 38] [204 68 103]] [0 0] [0 2]) =>
           [[669 533 51] [139 9 38] [204 68 103]])
  (fact "it solves a 3 X 3 to the end"
         (solve-maze-dfs [[157 21 51] [139 9 38] [204 68 103]] [0 0] [2 2]) =>
           [[669 533 1075] [139 1033 2086] [204 580 103]]))

(facts "about where solution bits should be in dfs-solver"
  (fact "solution bits in at most one direction"
         (more-than-one-solution? sm_ten) => false
         (more-than-one-solution? sm_nine) => false
         (more-than-one-solution? sm_eight) => false
         (more-than-one-solution? sm_seven) => false
         (more-than-one-solution? sm_seven_by_six) => false
         (more-than-one-solution? sm_six) => false
         (more-than-one-solution? sm_five) => false))

(facts "about wall and solution bits in dfs-solver"
  (fact "there can be no wall and solution bit in the same direction"
         (solution-at-wall? sm_ten) => false
         (solution-at-wall? sm_nine) => false
         (solution-at-wall? sm_eight) => false
         (solution-at-wall? sm_seven) => false
         (solution-at-wall? sm_seven_by_six) => false
         (solution-at-wall? sm_six) => false
         (solution-at-wall? sm_five) => false))
